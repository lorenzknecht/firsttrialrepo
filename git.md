#git

####Ein Repository klonen / initial herunterladen
```git clone benutzername@host:/pfad/zum/repository```

####Workflow
* Mit ```git status``` kann man jederzeit prüfen ob im lokalen WorkDir etwas geändert wurde.

Lokal gibt es drei Workflow-Punkte
* Working Directory: mit den echten Dateien. ```git add *``` bzw. ```git add <filename>``` bringen die neuen Dateien zum Index (stagen). Solange nichts commitet wurde kann man Änderungen beliebigt stagen.
* Index: Aus dem index können die Dateien zurück ins Working Directory geholt werden (unstagen), oder aber in den Head gebracht werden: ```git commit -m "Commit-Nachricht"``` (committen).
* Head: Aus dem commit wurden die Änderungen in den Head des lokalen Repositories gebracht.

####Daten hochladen
* Aus dem Head des lokalen Repos können die Änderungen nun hochgeladen werden (pushen):
```git push origin <branchname>```


####Daten vom git abrufen bzw. aktualiseren
* ```git fetch``` - Zeigt nur an ob es von remote -> local was zu aktualisieren braucht. Dabei wird nichts kopiert.
* umgekehrt hilft ```git status```.
* ```git pull``` verbindet ```git fetch``` und ```git merge```. 

####Feature Branch zum Master Branch mergen (remote)

* ```git checkout master```
* ```git pull origin master```
* ```git merge --squash featureBranchName```
* ```git commit```
* ```git push origin master```

####Eine Branch auf einen anderen kopieren (remote)
* thisBranch auf thoseBranch kopieren

* ```git checkout <thisBranch>```
* ```git rebase <thoseBranch>``` 

####Ein Repository auschecken
```git checkout <branchname>``` führt dazu, dass lokal ein anderer Branch in den Fokus genommen werden kann. Dabei ändert sich dann auch der Zustand im lokalen WorkDir.
